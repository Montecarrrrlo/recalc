import grpc
from concurrent import futures
import time

# import the generated classes
import calculator_pb2
import calculator_pb2_grpc

# create a class to define the server functions, derived from
# calculator_pb2_grpc.CalculatorServicer
class CalculatorServicer(calculator_pb2_grpc.CalculatorServicer):

    # calculator.square_root is exposed here
    # the request and response are of the data type
    # calculator_pb2.Number
    #
    # def SquareRoot(self, request, context):
    #     response = calculator_pb2.Number()
    #     response.value = calculator.square_root(request.value)
    #     return response

    def add(self, request, context):
        response = calculator_pb2.Number()
        response.value = sum(request.numbers)
        print("The value of addition is {:.2f}".format(response.value))
        return response
    
    def sub(self, request, context):
        response = calculator_pb2.Number()
        response.value = request.numbers[0] - sum(request.numbers[1:])
        print("The value of subtraction is {:.2f}".format(response.value))
        return response
    
    def div(self, request, context):
        tmp = request.numbers[0]
        for i in request.numbers[1:]:
            tmp /= i
        print("The value of division is {:.2f}".format(tmp))
        return calculator_pb2.Number(value=tmp)
    
    def mult(self, request, context):
        tmp = request.numbers[0]
        for i in request.numbers[1:]:
            tmp *= i
        print("The value of multiplication is {:.2f}".format(tmp))
        return calculator_pb2.Number(value=tmp)
    
    def pow(self, request, context):
        tmp = request.numbers[0]
        for i in request.numbers[1:]:
            tmp = pow(tmp, i)
        print("The value of power is {:.2f}".format(tmp))
        return calculator_pb2.Number(value=tmp)
    
    def root(self, request, context):
        tmp = request.numbers[0]
        for i in request.numbers[1:]:
            tmp = pow(tmp, 1/i)
        print("The value of root is {:.2f}".format(tmp))
        return calculator_pb2.Number(value=tmp)

# create a gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

# use the generated function `add_CalculatorServicer_to_server`
# to add the defined class to the server
calculator_pb2_grpc.add_CalculatorServicer_to_server(
        CalculatorServicer(), server)

# listen on port 50051
print('Starting server. Listening on port 50051.')
server.add_insecure_port('[::]:50051')
server.start()

# since server.start() will not block,
# a sleep-loop is added to keep alive
try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)