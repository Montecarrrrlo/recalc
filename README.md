# ReCalc

This is an example for using Google Remote Procedure Call (GRPC) library. It defines a simple remote calculator that provides simple arithmetic through the usage of remote procedure call library. For more information of GRPC library, visit [grpc.io](grpc.io).

## Structure

`calculator.proto` provides language-neutral interface definition, include two message types, `Number` and `ListNumbers`, and one service, `Calculator`.

- Message `Number` contains only a single floating point number in `Number.value`.
- Message `ListNumbers` contains a iterable that represents a list of floating point numbers in `ListNumbers.numbers`.
- Service `Calculator` defines a set of simple arithmetic: addition, subtraction, division, multiplication, power, and root. All taking `ListNumber` as input and returns `Numbers`.

`calculator_pb2.py` includes metadata and descriptors for `calculator.proto` in Python.

`calculator_pb2_grpc.py` includes the detailed interface and Python classes for types and method defined in `calculator.proto`.

`protoc.sh` is the bash code used to compile `calculator.proto` to the above two Python file.

`server.py` includes the detailed implementation for the calculator

- `CalculatorServiser` contains the detailed implementation for the `calculator_pb2_grpc.CalculatorServicer` , which represents the `Calculator` service defined in `calculator.proto`.
- The rest of the file contains the startup code for the server. 

`client.py` simulates client calls to the service provided by the server.

## Usage

1. compile `calculator.proto` by running `protoc.sh`. `calculator_pb2.py` and `calculator_pb2_grpc.py` will show under the directory if the compilation is success.

2. run `python server.py` to use the server.
3. Change the address or port in line 8 of `client.py` to your server's IP address and port. Note that the server should be accessible from outside if you are using the service remotely. For local enviornment, use `localhost:<port>` or `127.0.0.1:<port>`.
4. run `python client.py` in another terminal/machine to run the server. The server and client will both print out the results of calculation.