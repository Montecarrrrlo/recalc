# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import calculator_pb2 as calculator__pb2


class CalculatorStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.add = channel.unary_unary(
        '/Calculator/add',
        request_serializer=calculator__pb2.ListNumbers.SerializeToString,
        response_deserializer=calculator__pb2.Number.FromString,
        )
    self.sub = channel.unary_unary(
        '/Calculator/sub',
        request_serializer=calculator__pb2.ListNumbers.SerializeToString,
        response_deserializer=calculator__pb2.Number.FromString,
        )
    self.div = channel.unary_unary(
        '/Calculator/div',
        request_serializer=calculator__pb2.ListNumbers.SerializeToString,
        response_deserializer=calculator__pb2.Number.FromString,
        )
    self.mult = channel.unary_unary(
        '/Calculator/mult',
        request_serializer=calculator__pb2.ListNumbers.SerializeToString,
        response_deserializer=calculator__pb2.Number.FromString,
        )
    self.pow = channel.unary_unary(
        '/Calculator/pow',
        request_serializer=calculator__pb2.ListNumbers.SerializeToString,
        response_deserializer=calculator__pb2.Number.FromString,
        )
    self.root = channel.unary_unary(
        '/Calculator/root',
        request_serializer=calculator__pb2.ListNumbers.SerializeToString,
        response_deserializer=calculator__pb2.Number.FromString,
        )


class CalculatorServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def add(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def sub(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def div(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def mult(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def pow(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def root(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_CalculatorServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'add': grpc.unary_unary_rpc_method_handler(
          servicer.add,
          request_deserializer=calculator__pb2.ListNumbers.FromString,
          response_serializer=calculator__pb2.Number.SerializeToString,
      ),
      'sub': grpc.unary_unary_rpc_method_handler(
          servicer.sub,
          request_deserializer=calculator__pb2.ListNumbers.FromString,
          response_serializer=calculator__pb2.Number.SerializeToString,
      ),
      'div': grpc.unary_unary_rpc_method_handler(
          servicer.div,
          request_deserializer=calculator__pb2.ListNumbers.FromString,
          response_serializer=calculator__pb2.Number.SerializeToString,
      ),
      'mult': grpc.unary_unary_rpc_method_handler(
          servicer.mult,
          request_deserializer=calculator__pb2.ListNumbers.FromString,
          response_serializer=calculator__pb2.Number.SerializeToString,
      ),
      'pow': grpc.unary_unary_rpc_method_handler(
          servicer.pow,
          request_deserializer=calculator__pb2.ListNumbers.FromString,
          response_serializer=calculator__pb2.Number.SerializeToString,
      ),
      'root': grpc.unary_unary_rpc_method_handler(
          servicer.root,
          request_deserializer=calculator__pb2.ListNumbers.FromString,
          response_serializer=calculator__pb2.Number.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'Calculator', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
