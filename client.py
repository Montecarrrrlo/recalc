import grpc

# import the generated classes
import calculator_pb2
import calculator_pb2_grpc

# open a gRPC channel
channel = grpc.insecure_channel('18.188.108.191:50051')

# create a stub (client)
stub = calculator_pb2_grpc.CalculatorStub(channel)

# create a valid request message
ln = calculator_pb2.ListNumbers(numbers=[28,2])

# make the call
response = stub.add(ln)
# et voilà
print(response.value)

# make the call
response = stub.sub(ln)
# et voilà
print(response.value)

# make the call
response = stub.div(ln)
# et voilà
print(response.value)

# make the call
response = stub.mult(ln)
# et voilà
print(response.value)

# make the call
response = stub.pow(ln)
# et voilà
print(response.value)

# make the call
response = stub.root(ln)
# et voilà
print(response.value)